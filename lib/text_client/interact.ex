defmodule TextClient.Interact do
  @moduledoc false

  alias TextClient.{Player, State}

  @compile if Mix.env == :test, do: :export_all

  def start() do
    Hangman.new_game()
    |> setup_state()
#    |> IO.inspect()     # whatever is passed in is also returned
    |> Player.play()
  end

  defp setup_state(game) do
    %State{game_service: game, tally: Hangman.tally(game)}
  end
  
end