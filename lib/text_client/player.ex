defmodule TextClient.Player do
  @moduledoc false

  alias TextClient.{Mover, Prompter, State, Summary}

  @compile if Mix.env == :test, do: :export_all

# after Hangman became a GenServer, game_service became a pid
#  def play(%State{tally: %{game_state: :won}, game_service: %{letters: letters}}) do
#    IO.puts "INSIDE game_state: :won"
#    exit_with_message "You WON!  Letters to guess were: #{letters}"
#  end
  
#  def play(%State{tally: %{game_state: :lost}, game_service: %{letters: letters}}) do
#    IO.puts "INSIDE game_state: :lost"
#    exit_with_message "Sorry, you lost!  Letters to guess were: #{letters}"
#  end

  def play(%State{tally: %{game_state: :won}}) do
    IO.puts "INSIDE play/1 game_state: :won"
    exit_with_message "You WON!"
  end

  def play(%State{tally: %{game_state: :lost}}) do
    IO.puts "INSIDE play/1 game_state: :lost"
    exit_with_message "Sorry, you lost!"
  end

  def play(game = %State{tally: %{game_state: :good_guess}}) do
    IO.puts "INSIDE play/1 game_state: :good_guess"
    continue_with_message(game, "Good guess!")
  end

  def play(game = %State{tally: %{game_state: :bad_guess}}) do
    IO.puts "INSIDE play/1 game_state: :bad_guess"
    continue_with_message(game, "Sorry, that isn't in the word'")
  end

  def play(game = %State{tally: %{game_state: :have_used}}) do
    IO.puts "INSIDE play/1 game_state: :have_used"
    continue_with_message(game, "You've already used that letter")
  end

  def play(game) do
#    IO.puts "INSIDE play/1 game"
    continue game
  end

  defp continue_with_message(game, message) do
    IO.puts "INSIDE continue_with_message/2 game, message"
    IO.puts(message)
    continue game
  end

  defp continue(game) do
    IO.puts "INSIDE continue/1 game"
    game
    |> IO.inspect()
    |> Summary.display()
    |> Prompter.accept_move()
    |> Mover.make_move()
    |> play()
  end

  defp exit_with_message(message) do
    IO.puts message
    exit :normal
  end

end