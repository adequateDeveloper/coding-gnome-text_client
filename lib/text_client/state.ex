defmodule TextClient.State do
  @moduledoc false

  defstruct(game_service: nil, tally: nil, guess: "")

end