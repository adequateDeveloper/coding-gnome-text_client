defmodule TextClient.Mover do
  @moduledoc false

  alias TextClient.State

  def make_move(game) do
    new_gs = Hangman.make_move(game.game_service, game.guess)
    new_tally = Hangman.tally(new_gs)
    %State{game | game_service: new_gs, tally: new_tally}
  end

end