defmodule PlayerTest do
  use ExUnit.Case, async: true
  import ExUnit.CaptureIO
  alias TextClient.{Interact, Player}
  doctest Player

  setup do
    state =
      Hangman.new_game
      |> Interact.setup_state

    {:ok, new_state: state}
  end

  test "exit_with_message/1 exits normally" do
    message = "TEST_MESSAGE"

    fun = fn ->
      assert catch_exit(Player.exit_with_message(message)) == :normal
    end

    assert capture_io(fun) == message <> "\n"
  end

  test "play/1 exits normally when game_state is :won", %{new_state: state} do
    new_tally = %{state.tally | game_state: :won}
    new_state = %{state | tally: new_tally}

    fun = fn ->
      assert catch_exit(Player.play(new_state)) == :normal
    end

    assert capture_io(fun) == "You WON!\n"
  end

  test "play/1 exits normally when game_state is :lost", %{new_state: state} do
    new_tally = %{state.tally | game_state: :lost}
    new_state = %{state | tally: new_tally}

    fun = fn ->
      assert catch_exit(Player.play(new_state)) == :normal
    end

    assert capture_io(fun) == "Sorry, you lost!\n"
  end

#  test "play/1 continues when game_state is :good_guess", %{new_state: state} do
#    new_tally = %{state.tally | game_state: :good_guess}
#    new_state = %{state | tally: new_tally}
#
#    fun = fn ->
#      assert catch_exit(Player.play(new_state)) == :normal
#      Player.play(new_state) ;
#      exit :normal
#    end
#
#      IO.inspect Player.play(new_state)
#      Player.play(new_state)
#
#    assert capture_io(Player.play(new_state)) == "Good guess!\n"
#    assert capture_io(Player.play(new_state)) == "blah\n"
#    exit :normal
#  end

end
