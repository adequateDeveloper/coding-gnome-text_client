defmodule InteractTest do
  use ExUnit.Case, async: true
  alias TextClient.{Interact, State}
  doctest Interact

  setup do
    game = Hangman.new_game
    {:ok, new_game: game}
  end

  test "state returns correct struct" do
    expected_keys = [:__struct__, :game_service, :guess, :tally]

    state = %State{}
    actual_keys = Map.keys(state)
    assert actual_keys == expected_keys

    assert state.game_service == nil
    assert state.tally == nil
    assert state.guess == ""
  end

  test "setup_state/1 returns correct struct", %{new_game: game} do
    state = Interact.setup_state game

    assert is_map(state.tally)
    assert is_pid(state.game_service)

    tally = state.tally
    assert tally.turns_left == 7
    assert tally.game_state == :initializing
    assert is_list(tally.letters)
    assert Enum.all?(tally.letters, fn(it) -> it == "-" end)

    game = state.game_service
#    assert game.turns_left == 7
#    assert game.game_state == :initializing
#    assert is_map(game.used)
#    assert is_list(game.letters)
#    assert length(game.letters) > 0
#    assert Enum.all?(game.letters, fn(it) -> is_binary(it) end)

#    assert length(game.letters) == length(tally.letters)
  end

#  test "start/0 starts a new game" do
#    game = TextClient.start
#    assert game.guess == ""
#  end

end
