defmodule PrompterTest do
  use ExUnit.Case
  alias TextClient.{Prompter, State}
  doctest Prompter

  test "check_input/1 exits normally when :error is set" do
    _fun = fn ->
      assert Prompter.check_input({:error, "TEST"}, %State{}) == :normal
    end
  end

  test "check_input/2 exits normally when :eof is set" do
    _fun = fn ->
      assert Prompter.check_input(:eof, %State{}) == :normal
    end
  end

end
